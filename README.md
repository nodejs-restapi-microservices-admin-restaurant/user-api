# Documentation
* [User API](#user-api)

## User API
### GET /users
Returns all User instance.

Example response:
```json
[
    {
        "isAdmin": true,
        "_id": "5dd0713430a9086cae1b76b4",
        "email": "admin@test.com",
        "password": "$2b$10$TMLDo9vvdBagWID6QmlwEuqujGF.I0Bhx3JYhrkqvUPpsquNU2a46",
        "__v": 0
    }
]
```

### POST /users
Adds to User instance in a database.

|Param|Type|Description|
|--|--|--|
|email|`string`|User email.|
|password|`string`|User password.|
|isAdmin|`bool`|Сonfirm that the user is an admin.|

Example request:
```json
{
    "email": "admin3@test.com",
    "password": "2222",
    "isAdmin": true
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /users/:id
Returns a User instance by id.

Example response:
```json
{
    "isAdmin": true,
    "_id": "5dd0713430a9086cae1b76b3",
    "email": "admin3@test.com",
    "password": "$2b$10$TMLDo9vvdBagWID6QmlwEuqujGF.I0Bhx3JYhrkqvUPpsquNU2a46",
    "__v": 0
}
```

### DELETE /users/:id
Delete a User instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
